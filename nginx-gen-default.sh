#!/usr/bin/bash

sed -i "s/domain.com/$1/g" conf.d/default.conf
sed -i "s/domain.com/$1/g" docker-compose.yml
